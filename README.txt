How it works and what it does;

The finished module has a neat piece of javascript which shifts the display between menu and node, it also has an iphonehome page which like the normal home page lists paginated teasers of whatever has been promoted to be displayed there.  Unfortunately that module isn't this module...

This module has NO javascript, NO iphonehome page (although it links to it) and NO buttons to switch between menu and nodes - the reason being, that part was paid work and so doesn't belong to me any more.  What you're left with is the menu display system and the node display template...  Now this probably sounds pretty rubbish so far, but it's possibly a complete solution already for some people (well bar the lack of a homepage);  if perhaps your site is simple and your menu links to nodes rather than other menu items (which display content) then the module will display them correctly.  This is because the module takes a menu item which links to another menu item and displays the menu and not the node, it'll only display the node when the menu item links to it.

Originally my brief called for a separate menu system specifically for iPhone users (hence the currently empty menu tab, which I've left in just in case anyone wishes to still use it), however the main Drupal menu looks absolutely fine especially when it has the switch buttons to go between menu and nodes.  Okay so this module doesn't have those, but they're very easy to put in...


Ideally I'd like the community to add to this module as I feel any involvement from myself would possibly upset the relationship I have with the company which is currently paying me more than I make in my day job...  They are aware that I've made this release though, so I'm not going to get into any trouble!  ;o)