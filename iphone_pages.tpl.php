<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php  print $print["language"] ?>" xml:lang="<?php  print $print["language"] ?>">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, user-scalable=no" />
      <title><?php  print $print["title"] ?></title>
    <?php  print $print["head"] ?>
    <?php  print $print["robots_meta"] ?>
    <?php  print $print["base_href"] ?>
    <?php  print $print["css"] ?>
  </head>
  <body>
    <?php  print "<img class=\"iphone_pages-logo\" src=\"". theme_get_setting('logo') ."\" alt=\"\" />\n" ?>
    <div class="print-title"><?php  print $print["title"] ?></div>
    <div class="print-content"><?php  print $node->body; ?></div>
  </body>
</html>
