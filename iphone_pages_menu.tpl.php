<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php  print $print["language"] ?>" xml:lang="<?php  print $print["language"] ?>">
<head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, user-scalable=no" />
        <title><?php  print $print["title"] ?></title>
        <?php  print $print["base_href"] ?>
        <link rel="stylesheet" href="/<?php  print drupal_get_path('module', 'iphone_pages') ."/RoundRectangle.css" ?>" />
</head>
<body>
    <?php  print "<img class=\"iphone_pages-logo\" src=\"". theme_get_setting('logo'). "\" alt=\"\" />\n" ?>
    <?php  if (variable_get('iphone_pages_display_title', 0)) print "<div class=\"iphone_pages-title\">". $print["title"]. "</div>" ?>
    <a class="button" href="#searchForm">Search</a>
    <div class="print-content"><?php  print $print["body"] ?></div>
</body>
</html>
